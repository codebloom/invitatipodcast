var keystone = require('keystone');
var Types = keystone.Field.Types;
var Promise = require('promise');

/**
 * ExpiryToken Model
 * ==================
 */

var ExpiryToken = new keystone.List('ExpiryToken', {
	autokey: { from: 'name', path: 'key', unique: true },
});

ExpiryToken.add({
	token: { type: Types.Text, required: true, max: 255, default: "" },
	exp: {type: Types.Text, required: true, max: 255, default: ""},
	email: {type: Types.Email},
	type: { type: Types.Text, max: 255}
});


ExpiryToken.register();
