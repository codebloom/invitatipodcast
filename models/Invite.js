var keystone = require('keystone');
var Types = keystone.Field.Types;
var Promise = require('promise');

/**
 * Invite Model
 * ==================
 */

var Invite = new keystone.List('Invite', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Invite.add({
	ownerId: {type: Types.Text, required: true, max: 255, default: ""},
	inviteeId: {type: Types.Text, required: true, max: 255, default: ""},
	type: {type: Types.Text, required: true, max: 255, default: ""}
});

Invite.register();
