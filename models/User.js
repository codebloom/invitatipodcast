var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */
var User = new keystone.List('User');

User.add({
	name: { type: Types.Text, required: true, index: true, max: 255 },
	email: { type: Types.Email, initial: true, required: true, unique: true, index: true },
	image: { 
		type: Types.CloudinaryImage
	},
	page: { type: Types.Text, min: 0, max: 255 },
	facebook: { type: Types.Text, min: 0, max: 255 },
	location: { type: Types.Text, min: 0, max: 255 },
	phone: { type: Types.Text, min: 0, max: 255 },
	title: { type: Types.Text, min: 0, max: 255 },
}, 'Guest', {
	isGuest: { type: Types.Boolean, index: true },
	guestSubject: { type: Types.Text, min: 0, max: 255 },
	description: { type: Types.Html, wysiwyg: true, max: 2000 },
	rangeGuest: { type: Types.Text, min: 0, max: 511 },
	onlineGuest: { type: Types.Text, min: 0, max: 511 },
}, 'Host', {
	isHost: { type: Types.Boolean, index: true },
	subject:{ type: Types.Text, index: true, max: 255 },
	hostDescription: { type: Types.Text, index: true, max: 2000 },
	episodes:  { type: Types.Url, max: 512 },
	onlineHost: { type: Types.Text, min: 0, max: 511 },
	rangeHost: { type: Types.Text, min: 0, max: 511 },
}, 'Account Protection', {
	password: { type: Types.Password, initial: true, required: true },
	activated: { type: Types.Boolean, label: 'Activated', index: true, default: false },
}, 'Permissions', {
	isAdmin: { type: Types.Boolean, label: 'Can access Keystone', index: true, default: false },
});

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function () {
	return this.isAdmin;
});


/**
 * Relationships
 */
User.relationship({ ref: 'Post', path: 'posts', refPath: 'author' });


/**
 * Registration
 */
User.defaultColumns = 'name, email, isAdmin';
User.register();
