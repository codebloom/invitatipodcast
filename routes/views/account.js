var keystone = require('keystone');
var User = keystone.list('User');
var ExpiryToken = keystone.list('ExpiryToken');
var Promise = require('promise');
var crypto = require('crypto');
var Email = require('keystone-email');
var Serf = require('../serf');

var userUpdaterOptions = {
	flashErrors: true,
	fields: 'name, email, image, page, facebook, location, phone, title, isGuest, guestSubject, description, rangeGuest, onlineGuest, isHost, subject, hostDescription, episodes, onlineHost, rangeHost',
	errorMessage: 'There was a problem saving your account:',
};

var userUpdaterOptionsCreate = {
	flashErrors: true,
	fields: 'name, email, image, page, facebook, location, phone, title, isGuest, guestSubject, description, rangeGuest, onlineGuest, isHost, subject, hostDescription, episodes, onlineHost, rangeHost, password',
	errorMessage: 'There was a problem saving your account:',
};

var AccountController = {};

AccountController.index = (req, res, next) => {
	//the user is logged in, we take him to his account
	if (req.user) {
		req.params.accountId = req.user.id;
		return AccountController.edit(req, res, next);
	} else {
		return res.redirect('/login');
	}
}

AccountController.view = (req, res) => {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.section = 'account';

	
	view.on('get', (next) => {
		Serf.queryById('User', req.params.accountId)

		.then((theUser) => locals.user = theUser)
		.catch((err) => Serf.handleNotFound(res, err))
		.then(next);
	});
	
	view.render('account/view');
}

AccountController.create = (req, res) => {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.section = 'account';
	locals.formData = req.body;

	view.on('post', { action: 'create' }, (next) => {

		//check passwords
		Serf.checkValuesEqual("Parolele introduse nu sunt identice. Va rugam sa reintroduceti.",
			req.body.password, req.body.password_repeat)

		//Sanitize Input
		.then(() => Serf.sanitizeObj(req.body))

		//create the user
		.then((cleanBody) => Serf.createSingle(req, 'User', cleanBody, userUpdaterOptionsCreate))
		
		//sign in
		.then(() =>  Serf.doSignIn({
			email: req.body.email,
			password: req.body.password
		}, req, res))

		.then(() => {
			req.flash('success', 'Contul tau a fost creat cu success.');
			res.redirect('/account');
		})

		.catch((err) => Serf.flashDefaultError(err, req, next))
		.then(next);
	});

	view.render('account/new');
}

AccountController.edit = (req, res) => {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.section = 'account';

	//The user tries to edit a different acount
	if (req.user && req.user.id !== req.params.accountId) {
		return Serf.handleForbidden(res);
	}

	view.on('get', (next) => {
		//get the requested user
		Serf.queryById('User', req.params.accountId)

		.then((theUser) => locals.user = theUser)
		.catch((err) => Serf.handleNotFound(res, err))
		.then(next);
	})

	//this only gets called after view.render was called (tested)
	view.on('post', { action: 'edit' }, (next) => {

		//Sanitize Input
		Serf.sanitizeObj(req.body)

		//update the user
		.then((cleanBody) => Serf.updateSingle(req, 'User', {
			email: req.body.email
		}, cleanBody, userUpdaterOptions))

		// ReQuery the user to have latest version
		.then(() => Serf.queryById('User', req.params.accountId))

		.then((theUser) => {
			locals.user = theUser;
			return Serf.flashSuccess(req, "Schimbarile au fost salvate cu success!", next)
		})
		.catch((err) => Serf.flashDefaultError(err, req, next));
	});

	
	view.render('account/edit');
}

AccountController.login = (req, res, next) => {
	//the user is logged in, we take him to his account
	if (req.user) {
		res.redirect('/account');
		return;
	}

	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.section = 'account login';
	locals.formData = { email: req.body.email };

	view.on('post', { action: 'login' }, (next) => {
		//user requested sign in
		Serf.doSignIn(req.body, req, res)

		.then(() => {
			if (req.body.remember) {
				res.cookie('jwt' , Serf.createLoginToken(req));
			}
			res.status(303);
			res.set('Location', '/account')
			next();

		})
		.catch((err) => {
			locals.loginFailed = true;
			Serf.flashDefaultError(new Error("Login-ul a eșuat. Va rugam sa incercati inca o data."), req, next);
		});
	});

	view.render('account/login');
}


AccountController.logout = (req, res, next) => {
	Serf.doSignOut(req, res)
	.then(() => {
		res.clearCookie('jwt');
		res.redirect('/')
	});
}

AccountController.recoverPassword = (req, res, next) => {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.section = 'account login';
	locals.stage = "RECOVER";
	

	//user requests recovery token
	view.on('post', { action: 'recover' }, (next) => {

		//look for user with the given email
		Serf.query('User', {email: req.body.email})

		//reject if not found
		.then((users) => Serf.arrayNonEmpty(users,
			new Error("We couldn't find this email in the database. You can create an account <a href='/account/new'>here</a>.")
		))

		//create the token
		.then((users) => Promise.all([
				Serf.createSingle(req, 'ExpiryToken', {
					token: crypto.randomBytes(32).toString('hex'),
					exp: "" + (Date.now() + 1000 * 60 * 60 * 6), //6 hours
					type: "password_reset",
					email: users[0].email
				}),
				users
			])
		)

		//send the token email
		.then(([theToken, users]) => Serf.sendPasswordResetEmail(theToken, users))

		.then(() => {req.flash("success", "Am trimis un email cu linkul de resetare al parolei. Daca nu ati primit, asteptati 5 - 10 minute si incercati din nou.")})

		.catch((err) => {
			req.flash('error', err.message || err.detail.message);
			locals.formData = {email: req.body.email}
		})
		.then(next);
	});

	//user requests recovery token
	view.on('post', { action: 'reset' }, (next) => {

		req.body.password == req.body.password_repeat ?
			Promise.resolve() : Promise.reject(new Error("Parolele introduse nu sunt identice. Va rugam sa reintroduceti."))
		//make sure the token is still intact
		Serf.checkPasswordRecoveryToken(req.body.token)

		.then(token => Serf.updateSingle(req, 'User', {email: token.email}, {password: req.body.password}))

		.then(() => {req.flash("success", "Parola a fost resetata. Puteti face login <a href='/login'>aici</a>")})

		.catch((err) => {req.flash('error', err.message || err.detail.message)}).then(next);
	});

	
	view.on('get', (next) => {
		if (req.query.token) {
			Serf.checkPasswordRecoveryToken(req.query.token)
			.then(() => {
				locals.stage = "RESET";
				locals.token = req.query.token;
			})
			.catch((err) => {
				req.flash("error", err.message);
				locals.stage = "TOKEN_ERROR";
			})
			.then(next);
		} else next();
	});

	view.render('account/recover');
}


exports = module.exports = AccountController;
