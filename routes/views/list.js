var keystone = require('keystone');
var User = keystone.list('User');
var Promise = require('promise');
var config = require('../config');
var Parser = require('../../libs/parser');
var Serf = require('../serf');

var ListController = {};


ListController.index = (req, res, next) => {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.section = 'list';
	
	Serf.getUsersPage(
		Serf.createFilterQueryObject(req.query),
		Parser.toInt10(req.query.page, 0)
	)

	.then((theUsers) => Serf.decorateUsersWithInvitations(req, theUsers))

	.then((theUsers) => {
		locals.users = theUsers;
		view.render('list/index');
	}).catch(err => Serf.handleNotFound(res, err));
}


ListController.getUsersJSON = (req, res) => { 
	Serf.getUsersPage(
		Serf.createFilterQueryObject(req.query),
		Parser.toInt10(req.query.page, 0)
	)

	.then((theUsers) => Serf.decorateUsersWithInvitations(req, theUsers))

	// have and array of users, and we need to render it one by one, but alas, 
	// tiwg only renders asynchronously, so we need to convert it to an array of promises,
	// and then concatenate the results
	.then(theUsers => Promise.all(
		theUsers.map(user => Serf.renderTwigFile('templates/views/list/card.twig', {user, sessionUser: req.user}))
	))

	//expect to be an array of HTML strings
	.then(renderedUsers => res.send({
		status: "success",
		usersCount: renderedUsers.length,
		usersHTML: renderedUsers.reduce((acc, crt) => acc+crt, '')
	}))

	.catch((e) => {
		res.send({
			status: "error",
			message: "Retrieving users failed. Message: " + e.message
		})
	})
}


ListController.invite = (req, res, next) => {
	//undo should be bool, but it turns out to be "true" or "false"
	req.body.undo = req.body.undo == "true";

	//we use a variable to not break the chain
	var _secondStep = req.body.undo ? Serf.unInvite : Serf.invite;


	Serf.chechLoggedUser(req)
	// either create or remove invite
	.then(() => _secondStep(req))

	// check if
	.then(() => {
		//don't check match if invitation undo - pointless
		if (req.body.undo) return Promise.resolve(false);

		return Serf.checkInviteMatch({
			ownerId: req.user.id,
			inviteeId: req.body.inviteeId,
			type: req.body.type
		})
	})

	//should resolve with true if we have match
	.then((match) => {
		res.send({
			status: "success",
			match
		})
	})
	.catch((err) => {
		res.send({
			status: "error",
			message: err.message
		});
	});
}



exports = module.exports = ListController;