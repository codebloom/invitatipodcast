module.exports = {
	list: {
		page_size: 9
	},

	jwt: {
		secret:  process.env.jwt_secret || "some special kind of secret",
		expires: 1000 * 60 * 60 * 24 * 7 //one week
	}
}