var Promise = require('promise');
var keystone = require('keystone');
var config = require('./config');
var jwt = require('jwt-simple');
var Twig = require('twig');
var extend = require('extend');
var path = require('path');
var sanitizeHtml = require('sanitize-html');

var Serf = {};


/**
 * Create a single object and return it.
 */

Serf.createSingle = (req, modelName, body = {}, options = {}) => {
	return new Promise((resolve, reject) => {

		var Model = keystone.list(modelName);

		var model = new Model.model();
		var updater = model.getUpdateHandler(req);
		updater.process(body, options, (err) => {
			if (err) return reject(err);
			resolve(model);
		});
	});
}



/** 
 * Update a single object
 * @param {object} req 
 * @param {string} modelName 
 * @param {object} query 
 * @param {object} body 
 * @param {object} options 
 */
Serf.updateSingle = (req, modelName, query = {}, body = {}, options = {}) => {
	return new Promise((resolve, reject) => {
		keystone.list(modelName).model
		.findOne(query)
		.exec((err, result) => {
			if (err) return reject(err);

			var updater = result.getUpdateHandler(req);
			updater.process(body, options, (err) => {
				if (err) return reject(err);
				resolve(result);
			});
		});
	})
}


/**
 * Query the database
 */
Serf.query = (modelName, query) => {
	return new Promise((resolve, reject) => {
		keystone.list(modelName).model
		.find(query)
		.exec((err, result) => {
			if (err) return reject(err);
			resolve(result);
		});
	});
}

/**
 * Get a single document
 * @param {string} modelName 
 * @param {string} id 
 */
Serf.queryById = (modelName, id) => {
	return new Promise((resolve, reject) => {
		keystone.list(modelName).model.findById(id, (err, user) => {
			err ? reject(err) : resolve(user);
		});
	});
}



/**
 * Check if an array is not empty, and throw an error otherwise.
 * @param {array} array 
 * @param {error to throw} err 
 */
Serf.arrayNonEmpty = (array, err) => {
	return new Promise((resolve, reject) => {
		if(Array.isArray(array) && array.length > 0)
			return resolve(array);
		reject(err || new Error("Empty array exception"));
	});
}


/**
 * Check if a token exists, and is 
 */
Serf.checkPasswordRecoveryToken = (token) => {
	return new Promise((resolve, reject) => {

		keystone.list('ExpiryToken').model
		.find({token, type: "password_reset"})
		.exec((err, tokens) => {
			if (err) return reject(err);

			if (tokens.length == 0 || Date.now() > parseInt(tokens[0].exp) ) {
				return reject(new Error("The token is invalid. Please check your email again, or request another one"));
			}

			resolve(tokens[0]);
		});
	});
}


/**
* Send password reset email
*/
Serf.sendPasswordResetEmail = (token, users) => {

	return new Promise((resolve, reject) => {
		if (!process.env.MAILGUN_API_KEY || !process.env.MAILGUN_DOMAIN) {
			console.log('Unable to send email - no mailgun credentials provided');
			return reject(new Error('We were unable to send the confirmation email. Please try again later.'));
		}
	
		new keystone.Email({
			templateName: 'recover-password',
			transport: 'mailgun',
		}).send({
			to: users,
			from: {
				name: 'InvitatiPodcast',
				email: 'contact@invitatipodcast.com',
			},
			subject: 'Password reset email',
			token: token.token
		}, resolve);
	});
}


/**
 * Sign in a user
 */
Serf.doSignIn = (body, req, res) => {
	return new Promise((resolve, reject) => {
		keystone.session.signin(body, req, res, resolve, reject);
	});
};


//logout
Serf.doSignOut = (req, res) => {
	return new Promise((resolve, reject) => {
		keystone.session.signout(req, res, resolve);
	});
}

/**
 * Login token
 */
Serf.createLoginToken = (req) => {
	return jwt.encode({
		email: req.body.email,
		exp: Date.now() + config.jwt.expires
	}, config.jwt.secret);
}

Serf.validateJWT = (token) => {
	return new Promise((resolve, reject) => {
		var payload = jwt.decode(token, config.jwt.secret);
		if (Date.now() > payload.exp) {
			reject();
		} else {
			resolve(payload);
		}
	});
}

/**
 * Intended for password == password_repeat
 */
Serf.checkValuesEqual = (errMessage, a, b) => {
	return new Promise((resolve, reject) => {
		a == b ? resolve() : reject(errMessage || "Exception: Expected values are expected equal");
	});
}


Serf.chechLoggedUser = (req) => {
	return new Promise((resolve, reject) => {
		req.user ? resolve() : reject(new Error("User expected to be logged in"));
	});
}

/**
 * Flash error
 */
Serf.flashDefaultError = (err, req, next) => {
	if (err) req.flash('error', err.message || err.detail.message || "Error!");
	next();
}


/**
 * Flash success
 */
Serf.flashSuccess = (req, message, next) => {
	req.flash('success', message || 'Success!');
	next();
}


/**
 * List query object <intended to serve filtering>
 */

Serf.createFilterQueryObject = (q) => {
	var _query = {};

	switch (q.type && q.type.toUpperCase()) {
		case "GUESTS":
			_query.isGuest = true;
		break;

		case "HOSTS":
			_query.isHost = true;
	}

	if ( q.query ) {
		_query.$or = [
			'name', 'email', 'location',  'title',
			'guestSubject', 'description', 'rangeGuest', 'onlineGuest',
			'subject', 'hostDescription', 'onlineHost', 'rangeHost'
		].reduce(function(acc, key) {
			acc.push({
				[key]: { $regex: q.query, $options: 'i'}
			});
			return acc;
		}, []);
	}
	return _query;
}

/**
 * Get users: intended for the list
 */
Serf.getUsersPage = (_query, page = 0) => {
	_query = _query || {};

	return new Promise((resolve, reject) => {

		keystone.list('User').model
		.find(_query)
		.skip(config.list.page_size * (page-1))
		.limit(config.list.page_size)
		.exec((err, users) => {
			err ? reject(err) : resolve(users);
		});
	});
}

/**
 * Creates an invite for a user
 */

Serf.invite = (req) => {
	return Serf.query('Invite', {
		ownerId: req.user.id,
		inviteeId: req.body.inviteeId,
		type: req.body.type
	})
	.then(result => {
		if ( result.length ) return Promise.resolve();
		return Serf.createSingle(req, 'Invite', {
			ownerId: req.user.id,
			inviteeId: req.body.inviteeId,
			type: req.body.type
		});
	})
}



/**
 * Deletes invites if found
 */

Serf.unInvite = (req) => {
	return new Promise((resolve, reject) => {
		keystone.list('Invite').model
		.find({
			ownerId: req.user.id,
			inviteeId: req.body.inviteeId,
			type: req.body.type
		})
		.remove((err) => {
			if(err) reject();
			resolve();
		})
	})
}


/**
 * Return true if invite has a corresponding match
 */
Serf.checkInviteMatch = (inviteBody) => {
	return Serf.query('Invite', {
		ownerId: inviteBody.inviteeId,
		inviteeId: inviteBody.ownerId,
		type: {$ne: inviteBody.type}
	})
	.then(result => {
		return Promise.resolve(!!result.length)
	});
}



Serf.decorateUsersWithInvitations = (req, users) => {
	return new Promise((resolve, reject) => {
		//if not logged in, don't do anything
		if (!req.user) return resolve(users);

		Promise.all([
			Serf.query('Invite', {ownerId: req.user.id}),
			Serf.query('Invite', {inviteeId: req.user.id})
		])


		.then(([invitesAsOwner, invitesAsInvitee]) => {
			resolve(users.map((user) => {

				// was the session user invided by the current user?
				var sessionUserWasInvited = invitesAsInvitee.reduce((flags, crtInvitation) => {
					if(crtInvitation.ownerId == user.id) {
						var isInvitationAsGuest = crtInvitation.type == "GUEST";
						flags.asGuest = isInvitationAsGuest || flags.asGuest;
						flags.asHost = !isInvitationAsGuest || flags.asHost;
					}
					return flags;
				}, {
					asHost: false,
					asGuest: false
				});

				
				// did the session user invite the current user?
				var sessionUserInvited = invitesAsOwner.reduce((flags, crtInvitation) => {
					if(crtInvitation.inviteeId == user.id) {
						var isInvitationAsGuest = crtInvitation.type == "GUEST";
						flags.asGuest = isInvitationAsGuest || flags.asGuest;
						flags.asHost = !isInvitationAsGuest || flags.asHost;
					}
					return flags;
				}, {
					asHost: false,
					asGuest: false
				});


				user.invitedSessionUserAsGuest = sessionUserWasInvited.asGuest;
				user.invitedSessionUserAsHost = sessionUserWasInvited.asHost;
				user.isInvitedBySessionUserAsGuest = sessionUserInvited.asGuest;
				user.isInvitedBySessionUserAsHost = sessionUserInvited.asHost;

				user.matchAsGuest = user.isInvitedBySessionUserAsGuest && user.invitedSessionUserAsHost;
				user.matchAsHost = user.isInvitedBySessionUserAsHost && user.invitedSessionUserAsGuest;

				return user;
			}))
		})
	});
}


Serf.renderTwigFile = (the_path, locals) => {
	var options = extend({
		settings: { "twig options": { async: false} }
	}, locals)

	return new Promise((resolve, reject) => {
		Twig.renderFile(path.resolve(the_path), options, (err, html) => {
			if (err) reject(err);
			resolve(html);
		});
	});
}


Serf.handleForbidden = (res) => {
	return res.status(403).send(keystone.wrapHTMLError(`You don't have the rights to edit this account. <br /><br /> ${err.message}`));
}


Serf.handleNotFound = (res, err) => {
	return res.status(404).send(keystone.wrapHTMLError(`Exception: Not found. <br /><br /> ${err.message}`));
}

/**
 * Intended to sanitize the req.body object
 */
Serf.sanitizeObj = (obj) => {
	return new Promise((resolve, reject) => {
		if (typeof obj !== "object") return reject(new Error("Serf.sanitizeObj expects object as parameter"));

		resolve(
			Object.keys(obj).reduce((acc, crt)=> {
				acc[crt] = typeof obj[crt] === "string" ?
					sanitizeHtml(obj[crt]) : obj[crt];
				return acc;
			}, {})
		);
	});
}



exports = module.exports = Serf;