
exports.toInt10 = (_input, _default) => {
	try {
		return parseInt(_input, 10)
	} catch(e) {
		return _default;
	}
}