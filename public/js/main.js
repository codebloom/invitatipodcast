window.showBSModal = function self(options) {

    var options = $.extend({
            title : '',
            body : '',
            remote : false,
            backdrop : 'static',
            size : false,
            onShow : false,
            onHide : false,
			actions : false,
			dismissable: true,
			contentClass: ""
        }, options);

    self.onShow = typeof options.onShow == 'function' ? options.onShow : function () {};
    self.onHide = typeof options.onHide == 'function' ? options.onHide : function () {};

    if (self.$modal == undefined) {
        self.$modal = $(`<div class="modal fade"><div class="modal-dialog"><div class="modal-content"></div></div></div>`).appendTo('body');
        self.$modal.on('shown.bs.modal', function (e) {
            self.onShow.call(this, e);
        });
        self.$modal.on('hidden.bs.modal', function (e) {
            self.onHide.call(this, e);
        });
	}
	
	if (options.contentClass || !self.$modal.hasClass(options.contentClass)) {
		self.$modal.addClass(options.contentClass);
	}

    var modalClass = {
        small : "modal-sm",
        large : "modal-lg"
    };

    self.$modal.data('bs.modal', false);
    self.$modal.find('.modal-dialog').removeClass().addClass('modal-dialog ' + (modalClass[options.size] || ''));
    self.$modal.find('.modal-content').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">${title}</h4></div><div class="modal-body">${body}</div><div class="modal-footer"></div>'.replace('${title}', options.title).replace('${body}', options.body));

    var footer = self.$modal.find('.modal-footer');
    if (Object.prototype.toString.call(options.actions) == "[object Array]") {
        for (var i = 0, l = options.actions.length; i < l; i++) {
            options.actions[i].onClick = typeof options.actions[i].onClick == 'function' ? options.actions[i].onClick : function () {};
            $('<button type="button" class="btn ' + (options.actions[i].cssClass || '') + '">' + (options.actions[i].label || '{Label Missing!}') + '</button>').appendTo(footer).on('click', options.actions[i].onClick);
        }
	}
	
	if ( options.dismissable ) {
    	$('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>').appendTo(footer);
	}

    self.$modal.modal(options);
}


const URL_PARAMS = location.search.replace("?", "").split("&").reduce( (acc, crt) => {
	var _components = crt.split("=");
	acc[_components[0]] = _components[1] || "";
	return acc;
 }, {});


window.showErrorModal = function(message) {
	showBSModal({
		title: `<i class="material-icons">sentiment_very_dissatisfied</i> Eroare`,
		body: message,
		contentClass: "modal-error"
	});
}


$(function(){
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
	
			reader.onload = function (e) {
				var $inputScreen = $('.user-input-screen');
				$inputScreen.css('background-image', `url(${e.target.result})`);
				$inputScreen.find('.no-image').addClass('no-image--hidden');
			}
	
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$(".user-image-input").change(function(){
		readURL(this);
	});

	/*
	var _filterTimeout;
	const FILTER_TIMEOUT = 1000;
	$('.filter-form input').on('change keyup', function(evt) {
		if ( _filterTimeout ) clearTimeout(_filterTimeout);
		$('.filter-query').addClass('loading');

		_filterTimeout = setTimeout(function(){
			$('.filter-form').submit();
		}, FILTER_TIMEOUT);
	});*/


	$('[data-load-more]').on('click', function(evt) {
		evt.preventDefault();
		evt.stopPropagation();
		var $this = $(this);
		var _page = $this.data('nextPage') || 2;
		var _pageSize = $this.data('pageSize') || 2;
		if ($this.hasClass('loading')) return;

		$this.addClass('loading');

		$.get('/api/list', $.extend({
			page: _page
		}, URL_PARAMS))
		.done(function(response){

			if (response.status == "success") {
				if (response.usersCount < _pageSize) {
					$this.closest('.list-footer-panel').hide();
				}

				$('.user-container-inner').append(response.usersHTML);
				$this.removeClass('loading');

			} else {
				window.showErrorModal(response.message);
			}
		})
		.fail(function(err){
			window.showErrorModal(err.message);
		});

	});


	


	$(document).on('click', '[data-invite]', function(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		const $invite = $(this);

		const inviteData = $(this).data('invite');
		const $check = $(this).find('.check');
		const originalStatus = $invite.attr('data-invite-status');
		const undo = ["owner", "match"].includes(originalStatus);

		var _modal = undo ? {
			title: "Esti sigur ca vrei sa retragi invitatia?",
			body: "Utilizatorul n-o sa mai vada invitatia ta. Retragerea e subtila, fara notificari speciale.",
			contentClass: "modal-warning",
			buttonClass: "btn-warning",
			btnLabel: "Retrage"
		} : {
			title: "Esti sigur ca vrei sa inviti?",
			body: "Acest utilizator o sa vada invitatia ta.",
			contentClass: "modal-success",
			buttonClass: "btn-primary",
			btnLabel: "Invita"
		}

		var $modal = showBSModal({
			title: _modal.title,
			body: _modal.body,
			contentClass: _modal.contentClass,
			actions: [{
				label: _modal.btnLabel,
				cssClass: _modal.buttonClass,
				onClick: function() {
					if (window.lockApi) return;

					window.lockApi = true;
					$.post('/api/invite', $.extend({
						undo
					}, inviteData))


					// invite request done
					.done((response) => {
						if (response.status == "error") {
							return showErrorModal(response.message);
						}

						if (undo) {
							$invite.attr('data-invite-status', originalStatus == 'match' ? 'invitee' : 'none' );
						} else {
							$invite.attr('data-invite-status', response.match ? 'match' : 'owner' );
						}

						$('.modal').modal("hide")

					}).fail(function(err) {
						window.showErrorModal(err.message);
					}).always(function(){
						window.lockApi = false;
					})
				}
			}]
		});

	});

	$('.cryptic-hpt').hide();


	const TOP_SCROLL_THRESHOLD = 20;
	const SCROLL_CLASSES = {
		top: 'scroll-position--top',
		body: 'scroll-position--body'
	}

	const applyScrollClass = () => {
		if ($(window).scrollTop() < TOP_SCROLL_THRESHOLD) {
			$('body').removeClass(SCROLL_CLASSES.body).addClass(SCROLL_CLASSES.top);
		} else {
			$('body').removeClass(SCROLL_CLASSES.top).addClass(SCROLL_CLASSES.body);
		}
	}

	$(window).on('scroll', applyScrollClass);
	applyScrollClass();



});


var elements = document.querySelectorAll('textarea:not(.prevent-rich-text)');
var editor = new MediumEditor(elements);